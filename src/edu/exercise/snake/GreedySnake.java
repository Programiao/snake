package edu.exercise.snake;

import android.os.Bundle;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GreedySnake extends Activity {

	private Button mStart;
	private GamingView mGameView;
	private static int width;
	private static int height;
	private static String ICICLE_KEY = "snake-view";
	private LinearLayout mPauseMenu;
	private Button mContinue;
	private Button mReturn;
	private Button mPause;
	private Button mRestart;
	private RelativeLayout mMainMenu;
	private TextView mScore;
	private ImageView mLose;
	private Button mSound;
	private Button mAbout;
	private Button mHelp;
	private ImageView mInfo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("debug", "create");
		// 取消标题栏,全屏
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		width = dm.widthPixels;
		height = dm.heightPixels;
		setContentView(R.layout.activity_greedy_snake);

		mGameView = (GamingView) findViewById(R.id.gaming);
		mMainMenu = (RelativeLayout) findViewById(R.id.mainmenu);

		mScore = (TextView) findViewById(R.id.score);

		mLose = (ImageView) findViewById(R.id.end);
		mLose.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				mLose.setVisibility(View.GONE);
			}

		});

		mSound = (Button) findViewById(R.id.sound);
		mSound.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				if (SoundPlayer.isMusicSt()) {
					SoundPlayer.setMusicSt(false);
					SoundPlayer.setSoundSt(false);
					mSound.setText(R.string.soundoff);
				} else {
					SoundPlayer.setMusicSt(true);
					SoundPlayer.setSoundSt(true);
					mSound.setText(R.string.soundon);
				}
			}

		});

		mInfo = (ImageView) findViewById(R.id.help);
		mInfo.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				mInfo.setVisibility(View.GONE);
			}

		});

		mAbout = (Button) findViewById(R.id.aboutbutton);
		mAbout.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				mInfo.setImageResource(R.drawable.about);
				mInfo.setVisibility(View.VISIBLE);
			}

		});

		mHelp = (Button) findViewById(R.id.helpbutton);
		mHelp.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				mInfo.setImageResource(R.drawable.help);

				mInfo.setVisibility(View.VISIBLE);
			}

		});

		mPause = (Button) findViewById(R.id.pausebutton);
		mPauseMenu = (LinearLayout) findViewById(R.id.pausemenu);

		mPause.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				mGameView.setMode(GamingView.PAUSE);
			}

		});

		// 开始游戏按钮
		mStart = (Button) findViewById(R.id.start);
		mStart.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// 切换到游戏
				mGameView.restart();
				mMainMenu.setVisibility(View.GONE);
			}

		});

		mContinue = (Button) findViewById(R.id.continuebutton);
		mContinue.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				mGameView.setMode(GamingView.RUNNING);
			}

		});

		mReturn = (Button) findViewById(R.id.returnbutton);
		mReturn.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				mGameView.setMode(GamingView.READY);
				mMainMenu.setVisibility(View.VISIBLE);
			}

		});
		

		mRestart = (Button) findViewById(R.id.restartbutton);
		mRestart.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				mGameView.setMode(GamingView.RUNNING);
				mGameView.restart();
			}

		});

		mGameView.initGame(width, height, mScore, mLose, mPauseMenu);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.activity_greedy_snake, menu);
		return true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		// Pause the game along with the activity

		if (mGameView.getMode() != GamingView.READY) {
			mGameView.setMode(GamingView.PAUSE);
			mPauseMenu.setVisibility(View.VISIBLE);
		}
		SoundPlayer.pauseMusic();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mGameView != null) {
			SoundPlayer.startMusic();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Store the game state
		outState.putBundle(ICICLE_KEY, mGameView.saveState());
	}

	@Override
	public void onRestoreInstanceState(Bundle outState) {
		// Store the game state
		mGameView.restoreState(outState.getBundle(ICICLE_KEY));
	}
	
	 @Override
     public void onBackPressed() {
             
             this.finish();
             Log.d("back","pressed");
             super.onBackPressed();
     }

}
