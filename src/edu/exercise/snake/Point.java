package edu.exercise.snake;

public class Point {
	public int x;
    public int y;

    public Point(int newX, int newY) {
        x = newX;
        y = newY;
    }

    public boolean equals(Point other) {
        if (x == other.x && y == other.y) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Coordinate: [" + x + "," + y + "]";
    }

}


