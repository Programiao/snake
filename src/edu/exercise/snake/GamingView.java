package edu.exercise.snake;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GamingView extends View {
	// 格子的大小
	private int DEFALT_GRID_SIZE = 40;
	private int mGridSize = DEFALT_GRID_SIZE;
	private int mOffsetX = 0;
	private int mOffsetY = 0;
	private int mColNum = 0;
	private int mRowNum = 0;
	private int mWidth = 0;
	private int mHeight = 0;
	public boolean mLose = false;

	// 游戏状态控制
	public static final int PAUSE = 0;
	public static final int READY = 1;
	public static final int RUNNING = 2;
	public static final int LOSE = 3;
	private int mMode = RUNNING;

	// item代号
	public static final int EMPTY = 0;

	public static final int SNAKE_HEAD_UP = 1;
	public static final int SNAKE_HEAD_DOWN = 2;
	public static final int SNAKE_HEAD_LEFT = 3;
	public static final int SNAKE_HEAD_RIGHT = 4;

	public static final int SNAKE_TAIL_UP = 5;
	public static final int SNAKE_TAIL_DOWN = 6;
	public static final int SNAKE_TAIL_LEFT = 7;
	public static final int SNAKE_TAIL_RIGHT = 8;

	public static final int SNAKE_BODY_X = 9;
	public static final int SNAKE_BODY_Y = 10;
	public static final int SNAKE_BODY_LEFTUP = 11;
	public static final int SNAKE_BODY_LEFTDOWN = 12;
	public static final int SNAKE_BODY_RIGHTUP = 13;
	public static final int SNAKE_BODY_RIGHTDOWN = 14;

	public static final int APPLE1 = 15;
	public static final int APPLE2 = 16;
	public static final int APPLE3 = 17;
	public static final int BERRY1 = 18;
	public static final int BERRY2 = 19;
	public static final int BERRY3 = 20;
	public static final int WATER = 21;
	public static final int BANANA = 22;
	// 图片的数目,增加道具后应增加此常量
	public static final int TOTAL_GRID_TYPE = 22;
	// 存放图片的数组
	private Drawable[] mGridImage;
	private Bitmap mbg;

	// 蛇的方向
	private static final int UP = 1;
	private static final int DOWN = 2;
	private static final int LEFT = 3;
	private static final int RIGHT = 4;
	private int mDirection = DOWN;
	private int mNextDirection = DOWN;

	// 分数
	private long mScore = 0;
	// 速度控制
	private long mMoveDelay = 300;
	// 上次移动的时间,与 mMoveDelay 比较来判断是否需要下次移动
	private long mLastMove;

	// 记录蛇和道具的位置
	private ArrayList<Grid> mSnake = new ArrayList<Grid>();
	private ArrayList<Grid> mAppleList = new ArrayList<Grid>();

	// 随机
	private static final Random RNG = new Random();
	private final Paint mPaint = new Paint();

	private TextView mScoreText;
	private ImageView mLoseImage;
	private LinearLayout mPauseMenu;

	// 刷新
	private RefreshHandler mRedrawHandler = new RefreshHandler();

	class RefreshHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			GamingView.this.update();
		}

		public void sleep(long delayMillis) {
			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), delayMillis);
		}
	};

	// 构造函数
	public GamingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		Log.d("debug", "constructor");
		mGridImage = new Drawable[TOTAL_GRID_TYPE + 1];
		SoundPlayer.init(context);
		SoundPlayer.startMusic();
	}

	public GamingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		Log.d("debug", "constructor");
		mGridImage = new Drawable[TOTAL_GRID_TYPE + 1];
		SoundPlayer.init(context);
		SoundPlayer.startMusic();
	}

	public void initGame(int width, int height, TextView scoreText,
			ImageView lose, LinearLayout pauseMenu) {

		Log.d("debug", "initgame");
		mScoreText = scoreText;
		mLoseImage = lose;
		mPauseMenu = pauseMenu;

		mWidth = width;
		mHeight = height;
		mColNum = (int) Math.floor(width / mGridSize);
		mRowNum = (int) Math.floor(height / mGridSize);

		Log.d("debug", "" + width);
		Log.d("debug", "" + height);

		mOffsetX = ((width - (mGridSize * mColNum)) / 2);
		mOffsetY = ((height - (mGridSize * mRowNum)) / 2);

		// 加载图片
		initImage();

		// 添加触摸事件监听
		setOnTouchListener(new View.OnTouchListener() {

			private float downx = 0;
			private float downy = 0;

			public boolean onTouch(View v, MotionEvent event) {
				if(mSnake.size() == 0)
					return false;
				// Log.d("debug","touch");
				int action = event.getAction();
				switch (action) {
				case (MotionEvent.ACTION_DOWN):
					downx = event.getX();
					downy = event.getY();

					int snakex = mSnake.get(mSnake.size() - 1).getCenterX();
					int snakey = mSnake.get(mSnake.size() - 1).getCenterY();

					int deltax = (int) downx - snakex;
					int deltay = (int) downy - snakey;

					if (Math.abs(deltax) > Math.abs(deltay)) {// 横向移动
						if (deltax < 0) {// left
							if (mDirection != RIGHT) {
								mNextDirection = LEFT;
							} else {// 不能向右,
								if (deltay < 0) {
									mNextDirection = UP;
								} else if (deltay > 0) {
									mNextDirection = DOWN;
								}
							}
						} else {
							if (mDirection != LEFT) {
								mNextDirection = RIGHT;
							} else {
								if (deltay < 0) {
									mNextDirection = UP;
								} else if (deltay > 0) {
									mNextDirection = DOWN;
								}
							}
						}
					} else {// 纵向移动
						if (deltay < 0) {
							if (mDirection != DOWN) {
								mNextDirection = UP;
							} else if (deltax < 0) {
								mNextDirection = LEFT;
							} else if (deltax > 0) {
								mNextDirection = RIGHT;
							}
						} else {
							if (mDirection != UP) {
								mNextDirection = DOWN;
								Log.d("debug", "down");
							} else if (deltax < 0) {
								mNextDirection = LEFT;
							} else if (deltax > 0) {
								mNextDirection = RIGHT;
							}
						}
					}

					break;
				}

				return false;
			}

		});
	}

	public void restart() {
		mSnake.clear();
		mAppleList.clear();
		//
		mSnake.add(new Grid(7, 5, SNAKE_TAIL_DOWN));// TAIL
		mSnake.add(new Grid(8, 5, SNAKE_BODY_Y));
		mSnake.add(new Grid(9, 5, SNAKE_BODY_Y));
		mSnake.add(new Grid(10, 5, SNAKE_HEAD_DOWN));// HEAD
		mNextDirection = DOWN;
		mDirection = DOWN;

		RNG.setSeed(System.currentTimeMillis());
		// add items
		addItems(false);
		addItems(false);

		mLose = false;
		mMoveDelay = 400;
		mScore = 0;
		mScoreText.setText("0");
		mLoseImage.setVisibility(View.GONE);

		setMode(RUNNING);
		GamingView.this.invalidate();

		update();
	}

	private void initImage() {
		Resources r = this.getContext().getResources();

		Drawable bg = r.getDrawable(R.drawable.gamebg);
		bg.setBounds(0, 0, mWidth, mHeight);
		mbg = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(mbg);
		bg.draw(canvas);

		// Drawable grid = r.getDrawable(R.drawable.apple1);
		// grid.setBounds(0, 0, mGridSize, mGridSize);

		mGridImage[APPLE1] = r.getDrawable(R.drawable.apple1);
		mGridImage[APPLE2] = r.getDrawable(R.drawable.apple2);
		mGridImage[APPLE3] = r.getDrawable(R.drawable.apple3);
		mGridImage[BERRY1] = r.getDrawable(R.drawable.strawberry1);
		mGridImage[BERRY2] = r.getDrawable(R.drawable.strawberry2);
		mGridImage[BERRY3] = r.getDrawable(R.drawable.strawberry3);

		mGridImage[SNAKE_BODY_LEFTDOWN] = r.getDrawable(R.drawable.snakebent);
		mGridImage[SNAKE_BODY_LEFTUP] = r.getDrawable(R.drawable.snakebent3);
		mGridImage[SNAKE_BODY_RIGHTDOWN] = r.getDrawable(R.drawable.snakebent1);
		mGridImage[SNAKE_BODY_RIGHTUP] = r.getDrawable(R.drawable.snakebent2);

		mGridImage[SNAKE_BODY_X] = r.getDrawable(R.drawable.snakebody);
		mGridImage[SNAKE_BODY_Y] = r.getDrawable(R.drawable.snakebody1);

		mGridImage[SNAKE_HEAD_DOWN] = r.getDrawable(R.drawable.snakehead2);
		mGridImage[SNAKE_HEAD_LEFT] = r.getDrawable(R.drawable.head);
		mGridImage[SNAKE_HEAD_RIGHT] = r.getDrawable(R.drawable.head1);
		mGridImage[SNAKE_HEAD_UP] = r.getDrawable(R.drawable.snakehead1);

		mGridImage[SNAKE_TAIL_DOWN] = r.getDrawable(R.drawable.snaketail2);
		mGridImage[SNAKE_TAIL_UP] = r.getDrawable(R.drawable.snaketail3);
		mGridImage[SNAKE_TAIL_LEFT] = r.getDrawable(R.drawable.snaketail1);
		mGridImage[SNAKE_TAIL_RIGHT] = r.getDrawable(R.drawable.snaketail);
		
		mGridImage[WATER] = r.getDrawable(R.drawable.water);
		mGridImage[BANANA] = r.getDrawable(R.drawable.banana);

		for (int i = 1; i < TOTAL_GRID_TYPE + 1; i++) {
			mGridImage[i].setBounds(0, 0, mGridSize, mGridSize);
		}

	}

	public void addItems(boolean item) {
		long now = System.currentTimeMillis();
		Log.d("add",""+(now - mLastMove));
		
		Grid newGrid = null;
		boolean found;
		do {
			// Choose a new location for our apple
			int newCol =(int)( 1 + RNG.nextDouble() * (mColNum - 4));
			int newRow = (int)(5 + RNG.nextDouble()* (mRowNum - 6));
			
			if(!item){
				//int t = RNG.nextInt(6);
				int x = (int)(RNG.nextDouble() * 6);
				newGrid = new Grid(newRow, newCol, APPLE1 + x);
			}else{
				int x = (int)(RNG.nextDouble() * 2);
				newGrid = new Grid(newRow, newCol, WATER + x);
			}
			found = false;
			// Make sure it's not already under the snake
			int snakelength = mSnake.size();
			for (int index = 0; index < snakelength; index++) {
				if (mSnake.get(index).equals(newGrid)) {
					found = true;
					break;
				}
			}
			for (int index = 0; index < mAppleList.size(); index++) {
				if (mAppleList.get(index).equals(newGrid)) {
					found = true;
					break;
				}
			}
		} while (found);

		mAppleList.add(newGrid);
		
		
		now = System.currentTimeMillis();
		Log.d("add end",""+(now - mLastMove));
	}

	public void update() {
		if (mMode == RUNNING) {
			long now = System.currentTimeMillis();
			
			Log.d("update",""+(now - mLastMove));

			
			if (now - mLastMove > mMoveDelay) {
				// 更新状态
				mLastMove = now;
				updateSnake();
				
				double t = Math.random() * 100;
				if(t < 2.0){			
					addItems(true);
				}
				
			}
			this.invalidate();	

			mRedrawHandler.sleep(mMoveDelay);
		}

	}

	public void setMode(int newMode) {
		int oldMode = mMode;
		mMode = newMode;

		if (newMode == RUNNING & oldMode != RUNNING) {
			if(mLose)
				restart();
			
			update();
			mPauseMenu.setVisibility(View.GONE);
			return;
		} else if (newMode == LOSE) {
			mLoseImage.setVisibility(View.VISIBLE);
			SoundPlayer.playSound(R.raw.exp);
			mLose = true;
		}
		else if(newMode == PAUSE){
			mPauseMenu.setVisibility(View.VISIBLE);
			mLoseImage.setVisibility(View.GONE);
		}else if(newMode == READY){
		}
		
	}

	public int getMode() {
		return mMode;
	}

	public void updateSnake() {
		boolean growSnake = false;

		// grab the snake by the head
		Grid head = mSnake.get(mSnake.size() - 1);
		int type = head.type;
		Grid newHead = new Grid(1, 1);

		switch (mNextDirection) {
		case RIGHT: {
			newHead = new Grid(head.row, head.col + 1, SNAKE_HEAD_RIGHT);
			switch (mDirection) {
			case RIGHT:
				type = SNAKE_BODY_X;
				break;
			case UP:
				type = SNAKE_BODY_RIGHTDOWN;
				break;
			case DOWN:
				type = SNAKE_BODY_RIGHTUP;
				break;
			}
			break;
		}
		case LEFT: {
			newHead = new Grid(head.row, head.col - 1, SNAKE_HEAD_LEFT);
			switch (mDirection) {
			case LEFT:
				type = SNAKE_BODY_X;
				break;
			case UP:
				type = SNAKE_BODY_LEFTDOWN;
				break;
			case DOWN:
				type = SNAKE_BODY_LEFTUP;
				break;
			}
			break;
		}
		case UP: {
			newHead = new Grid(head.row - 1, head.col, SNAKE_HEAD_UP);
			switch (mDirection) {
			case UP:
				type = SNAKE_BODY_Y;
				break;
			case LEFT:
				type = SNAKE_BODY_RIGHTUP;
				break;
			case RIGHT:
				type = SNAKE_BODY_LEFTUP;
				break;
			}
			break;
		}
		case DOWN: {
			newHead = new Grid(head.row + 1, head.col, SNAKE_HEAD_DOWN);
			switch (mDirection) {
			case DOWN:
				type = SNAKE_BODY_Y;
				break;
			case LEFT:
				type = SNAKE_BODY_RIGHTDOWN;
				break;
			case RIGHT:
				type = SNAKE_BODY_LEFTDOWN;
				break;
			}
			break;
		}
		}

		// Look for collisions with itself
		int snakelength = mSnake.size();
		for (int snakeindex = 0; snakeindex < snakelength; snakeindex++) {
			Grid c = mSnake.get(snakeindex);
			if (c.equals(newHead)) {
				Log.d("debug", "collision with itself");
				setMode(LOSE);
				return;
			}
		}

		// Collision detection
		// For now we have a 1-square wall around the entire arena
		if ((newHead.col < 1) || (newHead.row < 3)
				|| (newHead.col > mColNum - 2) || (newHead.row > mRowNum - 2)) {
			Log.d("debug", "out of bound");
			setMode(LOSE);
			return;

		}


		
		boolean shortSnake = false;
		
		// Look for apples
		int applecount = mAppleList.size();
		for (int appleindex = 0; appleindex < applecount; appleindex++) {
			Grid c = mAppleList.get(appleindex);
			if (c.equals(newHead)) {
				
				SoundPlayer.boom();
				mAppleList.remove(c);
				
				if(c.type == BANANA)
				{
					mMoveDelay *= 1.1;
					mScore += 10;
				}else if(c.type == WATER){
					shortSnake = true;
					mScore += 10;
					if(mSnake.size() < 3){
						setMode(LOSE);
						return;
					}
				}else{
					addItems(false);

					mScore += Math.ceil(1 / (mMoveDelay * 0.001) * 5);
					mMoveDelay *= 0.95;

					growSnake = true;

					CharSequence str = "" + mScore;
					mScoreText.setText(str);
				}
				break;
			}
		}


		
		if(!shortSnake)
		{
			mSnake.add(newHead);
			mSnake.get(mSnake.size()-2).type = type;
			mDirection = mNextDirection;
		}
		
		
		// except if we want the snake to grow
		if (!growSnake) {
			// Log.d("debug",mSnake.get(0).getPointPosition().toString());

			if (mSnake.get(1).type == SNAKE_BODY_X
					|| mSnake.get(1).type == SNAKE_BODY_Y) {
				mSnake.get(1).type = mSnake.get(0).type;
			} else {
				switch (mSnake.get(0).type) {
				case SNAKE_TAIL_UP:
					if (mSnake.get(1).type == SNAKE_BODY_LEFTDOWN) {
						mSnake.get(1).type = SNAKE_TAIL_LEFT;
					} else {
						mSnake.get(1).type = SNAKE_TAIL_RIGHT;
					}
					break;
				case SNAKE_TAIL_DOWN:
					if (mSnake.get(1).type == SNAKE_BODY_LEFTUP) {
						mSnake.get(1).type = SNAKE_TAIL_LEFT;
					} else {
						mSnake.get(1).type = SNAKE_TAIL_RIGHT;
					}
					break;
				case SNAKE_TAIL_LEFT:
					if (mSnake.get(1).type == SNAKE_BODY_RIGHTUP) {
						mSnake.get(1).type = SNAKE_TAIL_UP;
					} else {
						mSnake.get(1).type = SNAKE_TAIL_DOWN;
					}
					break;
				case SNAKE_TAIL_RIGHT:
					if (mSnake.get(1).type == SNAKE_BODY_LEFTUP) {
						mSnake.get(1).type = SNAKE_TAIL_UP;
					} else {
						mSnake.get(1).type = SNAKE_TAIL_DOWN;
					}
					break;
				}
			}
			mSnake.remove(0);
		}

	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		mColNum = (int) Math.floor(w / mGridSize);
		mRowNum = (int) Math.floor(h / mGridSize);

		mOffsetX = ((w - (mGridSize * mColNum)) / 2);
		mOffsetY = ((h - (mGridSize * mRowNum)) / 2);

	}

	private void drawGrid(Grid grid, Canvas canvas) {

		// canvas.drawRect(grid.getLeft(), grid.getTop(), grid.getRight(),
		// grid.getBottom(), mPaint);

		if (grid.type != 0) {
			Bitmap bitmap = Bitmap.createBitmap(mGridSize, mGridSize,
					Bitmap.Config.ARGB_8888);
			Canvas c = new Canvas(bitmap);
			c.drawBitmap(mbg, grid.getRect(), new Rect(0, 0, mGridSize,
					mGridSize), mPaint);
			mGridImage[grid.type].draw(c);
			canvas.drawBitmap(bitmap, grid.getLeft(), grid.getTop(), mPaint);
			bitmap.recycle();
		}

	}

	@Override
	public void onDraw(Canvas canvas) {

		long now = System.currentTimeMillis();
		Log.d("draw",""+(now - mLastMove));
		
		super.onDraw(canvas);
		Iterator<Grid> it = mSnake.iterator();
		while (it.hasNext()) {
			drawGrid(it.next(), canvas);
		}
		it = mAppleList.iterator();
		while (it.hasNext()) {
			drawGrid(it.next(), canvas);
		}

	}

	public Bundle saveState() {
		Bundle map = new Bundle();

		map.putIntArray("mAppleList", gridArrayListToArray(mAppleList));
		map.putInt("mDirection", Integer.valueOf(mDirection));
		map.putInt("mNextDirection", Integer.valueOf(mNextDirection));
		map.putLong("mMoveDelay", Long.valueOf(mMoveDelay));
		map.putLong("mScore", Long.valueOf(mScore));
		map.putIntArray("mSnake", gridArrayListToArray(mSnake));

		return map;
	}

	public void restoreState(Bundle icicle) {
		setMode(PAUSE);

		mAppleList = gridArrayToArrayList(icicle.getIntArray("mAppleList"));
		mDirection = icicle.getInt("mDirection");
		mNextDirection = icicle.getInt("mNextDirection");
		mMoveDelay = icicle.getLong("mMoveDelay");
		mScore = icicle.getLong("mScore");
		mSnake = gridArrayToArrayList(icicle.getIntArray("mSnake"));
	}

	private ArrayList<Grid> gridArrayToArrayList(int[] rawArray) {
		ArrayList<Grid> arrayList = new ArrayList<Grid>();

		int gridCount = rawArray.length;
		for (int index = 0; index < gridCount; index += 2) {
			Grid c = new Grid(rawArray[index], rawArray[index + 1],
					rawArray[index + 2]);
			arrayList.add(c);
		}
		return arrayList;
	}

	private int[] gridArrayListToArray(ArrayList<Grid> cvec) {
		int count = cvec.size();
		int[] rawArray = new int[count * 3];
		for (int index = 0; index < count; index++) {
			Grid c = cvec.get(index);
			rawArray[3 * index] = c.row;
			rawArray[3 * index + 1] = c.col;
			rawArray[3 * index + 2] = c.type;
		}
		return rawArray;
	}

	public class Grid {

		public int row;
		public int col;
		public int type;

		public Grid(int rowIndex, int colIndex, int itemType) {
			col = colIndex;
			row = rowIndex;
			type = itemType;
		}

		public Grid(int rowIndex, int colIndex) {
			col = colIndex;
			row = rowIndex;
			type = 0;
		}

		public Point getPointPosition() {
			return new Point(col * mGridSize + mOffsetX, row * mGridSize
					+ mOffsetY);
		}

		public int getCenterX() {
			return col * mGridSize + mOffsetX + mGridSize / 2;
		}

		public int getCenterY() {
			return row * mGridSize + mOffsetY + mGridSize / 2;
		}

		public int getLeft() {
			return col * mGridSize + mOffsetX;
		}

		public int getTop() {
			return row * mGridSize + mOffsetY;
		}

		public int getRight() {
			return (col + 1) * mGridSize + mOffsetX;
		}

		public int getBottom() {
			return (row + 1) * mGridSize + mOffsetY;
		}

		public Rect getRect() {
			return new Rect(getLeft(), getTop(), getRight(), getBottom());
		}

		public boolean equals(Grid other) {
			if (col == other.col && row == other.row) {
				return true;
			}
			return false;
		}

	}

}
